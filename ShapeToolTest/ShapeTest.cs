﻿using System;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace ShapeToolTest
{
    [TestClass]
    public class ShapeTest
    {
        [TestMethod]
        public void MultiparseTest()
        {

            ShapeTool.Form1 form = new ShapeTool.Form1();

            ShapeTool.Parser cmdSetup = new ShapeTool.Parser();
            Bitmap outBitmap = form.myBitmap;

            ShapeTool.Draw myDraw = new ShapeTool.Draw(Graphics.FromImage(outBitmap));
            cmdSetup.parse("run", "drawto 10,10", myDraw);
        }

        [TestMethod]
        public void ParameterSeperator()
        {
            ShapeTool.Form1 form = new ShapeTool.Form1();

            ShapeTool.Parser cmdSetup = new ShapeTool.Parser();
            Bitmap outBitmap = form.myBitmap;

            ShapeTool.Draw myDraw = new ShapeTool.Draw(Graphics.FromImage(outBitmap));
            cmdSetup.parse("run", "drawto 10,10", myDraw);

        }

        [TestMethod]
        public void MoveToTest()
        {
            ShapeTool.Form1 form = new ShapeTool.Form1();

            ShapeTool.Parser cmdSetup = new ShapeTool.Parser();
            Bitmap outBitmap = form.myBitmap;

            ShapeTool.Draw myDraw = new ShapeTool.Draw(Graphics.FromImage(outBitmap));
            cmdSetup.parse("moveto 10,10", "", myDraw);

        }

        [TestMethod]
        public void RectangleTest()
        {
            ShapeTool.Form1 form = new ShapeTool.Form1();

            ShapeTool.Parser cmdSetup = new ShapeTool.Parser();
            Bitmap outBitmap = form.myBitmap;

            ShapeTool.Draw myDraw = new ShapeTool.Draw(Graphics.FromImage(outBitmap));
            cmdSetup.parse("rectangle 10,10", "", myDraw);
            cmdSetup.parse("square 10", "", myDraw);
        }

        [TestMethod]
        public void CircleTest()
        {
            ShapeTool.Form1 form = new ShapeTool.Form1();

            ShapeTool.Parser cmdSetup = new ShapeTool.Parser();
            Bitmap outBitmap = form.myBitmap;

            ShapeTool.Draw myDraw = new ShapeTool.Draw(Graphics.FromImage(outBitmap));
            cmdSetup.parse("circle 10", "", myDraw);
        }

        [TestMethod]
        public void TriangleTest()
        {
            ShapeTool.Form1 form = new ShapeTool.Form1();

            ShapeTool.Parser cmdSetup = new ShapeTool.Parser();
            Bitmap outBitmap = form.myBitmap;

            ShapeTool.Draw myDraw = new ShapeTool.Draw(Graphics.FromImage(outBitmap));
            cmdSetup.parse("drawto 100,80,20", "", myDraw);
        }

        [TestMethod]
        public void ColorTest()
        {
            ShapeTool.Form1 form = new ShapeTool.Form1();

            ShapeTool.Parser cmdSetup = new ShapeTool.Parser();
            Bitmap outBitmap = form.myBitmap;

            ShapeTool.Draw myDraw = new ShapeTool.Draw(Graphics.FromImage(outBitmap));
            cmdSetup.parse("pen black", "", myDraw);
        }

        [TestMethod]
        public void FillTest()
        {
            ShapeTool.Form1 form = new ShapeTool.Form1();

            ShapeTool.Parser cmdSetup = new ShapeTool.Parser();
            Bitmap outBitmap = form.myBitmap;

            ShapeTool.Draw myDraw = new ShapeTool.Draw(Graphics.FromImage(outBitmap));
            cmdSetup.parse("fill on", "", myDraw);
        }

        [TestMethod]
        public void ClearTest()
        {
            ShapeTool.Form1 form = new ShapeTool.Form1();

            ShapeTool.Parser cmdSetup = new ShapeTool.Parser();
            Bitmap outBitmap = form.myBitmap;

            ShapeTool.Draw myDraw = new ShapeTool.Draw(Graphics.FromImage(outBitmap));
            cmdSetup.parse("clear", "", myDraw);
        }

        [TestMethod]
        public void ResetTest()
        {
            ShapeTool.Form1 form = new ShapeTool.Form1();

            ShapeTool.Parser cmdSetup = new ShapeTool.Parser();
            Bitmap outBitmap = form.myBitmap;

            ShapeTool.Draw myDraw = new ShapeTool.Draw(Graphics.FromImage(outBitmap));
            cmdSetup.parse("reset", "", myDraw);
        }

        [TestMethod]
        public void RunTest()
        {
            ShapeTool.Form1 form = new ShapeTool.Form1();

            ShapeTool.Parser cmdSetup = new ShapeTool.Parser();
            Bitmap outBitmap = form.myBitmap;

            ShapeTool.Draw myDraw = new ShapeTool.Draw(Graphics.FromImage(outBitmap));
            cmdSetup.parse("run", "", myDraw);
        }

        [TestMethod]
        public void VarStoreTest()
        {
            ShapeTool.Form1 form = new ShapeTool.Form1();
            ShapeTool.Parser cmdSetup = new ShapeTool.Parser();
            Bitmap outBitmap = form.myBitmap;

            ShapeTool.Draw MyDraws = new ShapeTool.Draw(Graphics.FromImage(outBitmap));
            cmdSetup.parse("value = 10", "", MyDraws);
        }

        [TestMethod]
        public void VarAppendTest()
        {
            ShapeTool.Form1 form = new ShapeTool.Form1();
            ShapeTool.Parser cmdSetup = new ShapeTool.Parser();
            Bitmap outBitmap = form.myBitmap;
            ShapeTool.Draw MyDraws = new ShapeTool.Draw(Graphics.FromImage(outBitmap));
            cmdSetup.parse("value = value + 10", "", MyDraws);
            cmdSetup.parse("value = value - 10", "", MyDraws);
            cmdSetup.parse("value = value / 10", "", MyDraws);
            cmdSetup.parse("value = value * 10", "", MyDraws);
        }
    }
}